console.log("Hello world");


let number = 2;
let getCube = number ** 3;

console.log(`The cube of ${number} is ${getCube}`);

let address = [258, "Honeymoon Ave.", "New York", 80001];

const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}.`);

let animal = {
 givenName: "Lolong",
 specie: "saltwater crocodile",
 weight: 1075,
 measurement: 10,
};

const {givenName, specie, weight, measurement} = animal
console.log(`${givenName} was a ${specie}. He weighed at ${weight} kgs with a measurement of ${measurement} m.`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((num) => {
 console.log(num);
});

let reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);


class Dog {
 constructor(name, age, breed){
this.name = name;
 this.age = age;
 this.breed = breed;
 }
}

const myDog = new Dog("Snoopy", 5, "Shiba Inu");
console.log(myDog);
